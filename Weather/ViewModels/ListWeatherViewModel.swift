//
//  MainViewModel.swift
//  Weather
//
//  Created by Maxim on 21.05.2022.
//

import Foundation
import RxSwift
import RxCocoa

class ListWeatherViewModel {
    let title = "Weather"
    var mainData: [MainWeather] = []{
        didSet{
            bind?(mainData)
        }
    }
    var bind: (([MainWeather]) -> Void)?
    var lat: Double = 0
    var long: Double = 0

    func fetch(){
    
        NetworkService.shared.fetch(url:URL(string:"https://api.openweathermap.org/data/2.5/onecall?lat=\(self.lat)&lon=\(self.long)&units=metric&exclude=minutely&appid=09733c70a5010225ffd555ea2e26a7b1")!) { (weather: MainWeather) in
            if self.mainData.isEmpty {
                self.mainData.insert(weather, at: 0)
            } else {
                self.mainData[0] = weather
            }
        }
    }
    
    func getCoordinats() {
        var _ = LocationManager.shared.coordinats.subscribe { (lat, long ) in
            self.lat = lat
            self.long = long
        }
    }
    
    
    func getLocationData(){
        LocationManager.shared.requestLocationAuthorization()
        var _ = LocationManager.shared.status.bind { status in
            if status.rawValue == 4 || status.rawValue == 5 {
                self.getCoordinats()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.fetch()
                }
            }
            print(status.rawValue)
        }
    }
}
