//
//  DetailViewController.swift
//  Weather
//
//  Created by Maxim on 04.06.2022.
//

import UIKit

class DetailViewController: UIViewController, Storyboardable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    var coordinator: AppCoordinator?
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var infoStack: UIStackView!
    @IBOutlet weak var plesure: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var smallInfo: UILabel!
    
    var cityData: MainWeather?
    
    let dateFormatterHours = { () -> DateFormatter in
        let dat = DateFormatter()
        dat.dateFormat = "HH"
        return dat
    }
    let dateFormatterDate = { () -> DateFormatter in
        let dat = DateFormatter()
        dat.dateFormat = "E"
        return dat
    }
    
    let headerViewMaxHeight : CGFloat = 230
    let headerViewMinHeight : CGFloat = 70
    var previousScrollOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView() {
        DispatchQueue.main.async {
            
            if self.cityData?.current?.weather?.first?.main?.rawValue == WeatherDescription.clear.rawValue {
                self.view.backgroundColor = UIColor(patternImage: UIImage(named: "clean")!)
            }else if self.cityData?.current?.weather?.first?.main?.rawValue == WeatherDescription.rain.rawValue {
                self.view.backgroundColor = UIColor(patternImage: UIImage(named: "rain")!)
            }else if self.cityData?.current?.weather?.first?.main?.rawValue == WeatherDescription.clouds.rawValue {
                self.view.backgroundColor = UIColor(patternImage: UIImage(named: "cloudy")!)
            }
        
            self.tableView.reloadData()
            if let sym = (self.cityData?.timezone?.range(of: "/")?.upperBound){
                let convert = self.cityData?.timezone?.suffix(from: sym).map { String($0) }.joined(separator: "")
                self.cityLabel.text = convert
            }
            self.temp.text = " \(String(format: "%.0f" , self.cityData?.current?.temp?.rounded() ?? -1))°"
            self.weatherDescription.text = self.cityData?.current?.weather?.first?.main?.rawValue
            self.plesure.text = "Feels Like : \(String(format: "%.0f" , self.cityData?.current?.feelsLike?.rounded() ?? -1))°"
            self.smallInfo.text = "\(String(format: "%.0f" , self.cityData?.current?.temp?.rounded() ?? -1))° | \(String(describing: self.cityData?.current?.weather?.first?.main?.rawValue ?? "empty"))"
        }
    }
    
    @IBAction func listButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DetailViewController {
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.heightView.constant - headerViewMinHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    func setScrollPosition() {
        self.tableView.contentOffset = CGPoint(x:0, y: 0)
    }
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (0...2).contains(section) {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! FirstTableViewCell
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondCell", for: indexPath) as! SecondTableViewCell
            cell.sect = self.cityData?.daily?.count ?? 0
            for x in 0..<cell.sect {
                cell.date.append(self.dateFormatterDate().string(from: Date(timeIntervalSince1970: TimeInterval(self.cityData?.daily?[x].dt ?? -1))))
                cell.icons.append(UIImage(named: self.cityData?.daily?[x].weather?.first?.icon ?? "01d")!)
                cell.minTemp.append(self.cityData?.daily?[x].temp?.min ?? -1)
                cell.maxTemp.append(self.cityData?.daily?[x].temp?.max ?? -1)
            }
            cell.insideTableView.reloadData()
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "thirdCell", for: indexPath) as! ThirdTableViewCell
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? FirstTableViewCell {
            cell.colectionView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 150
        case 1:
            return 370
        case 2:
            return 180
        default:
            return 0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = heightView.constant
            if isScrollingDown {
                newHeight = max(headerViewMinHeight, heightView.constant - abs(scrollDiff))
                if newHeight <= 170 {
                    infoStack.isHidden = true
                    smallInfo.isHidden = false
                }
            } else if isScrollingUp {
                newHeight = min(headerViewMaxHeight, heightView.constant + abs(scrollDiff))
                if newHeight >= 170 {
                    infoStack.isHidden = false
                    smallInfo.isHidden = true
                }
            }
            if newHeight != heightView.constant {
                heightView.constant = newHeight
                setScrollPosition()
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
}

extension DetailViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.cityData?.hourly?.count ?? 0

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourWeatherCell", for: indexPath) as! ForHourCollectionViewCell
        if indexPath.row == 0{
            cell.time.text = "Now"
        }else{
            cell.time.text = self.dateFormatterHours().string(from: Date(timeIntervalSince1970: TimeInterval(self.cityData?.hourly?[indexPath.row].dt ?? -1)))
        }
        cell.temp.text = "\(String(format: "%.0f" ,self.cityData?.hourly?[indexPath.row].temp ?? -1))°"
        cell.icon.image = UIImage(named: self.cityData?.hourly?[indexPath.row].weather?.first?.icon ?? "01d")
        
        return cell
    }
    
}
