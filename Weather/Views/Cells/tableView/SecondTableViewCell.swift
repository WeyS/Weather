//
//  SecondTableViewCell.swift
//  Weather
//
//  Created by Maxim on 07.06.2022.
//

import UIKit

class SecondTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var insideTableView: UITableView!
    
    var sect = 0
    var date: [String] = []
    var icons: [UIImage] = []
    var minTemp: [Double] = []
    var maxTemp: [Double] = []
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        insideTableView.dataSource = self
        insideTableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sect == 0{
            return 0
        }else{
            return sect
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "12", for: indexPath) as! SecondCustomTableViewCell
        if indexPath.row == 0 {
            cell.dateLable.text = "Now"
        }else{
            cell.dateLable.text = date[indexPath.row]
        }
        cell.icon.image = icons[indexPath.row]
        cell.minTemp.text = "\(String(format: "%.0f" ,minTemp[indexPath.row]))°"
        cell.maxTemp.text = "\(String(format: "%.0f" ,maxTemp[indexPath.row]))°"
        
        if let gradientImage = UIImage.gradientImage(with: cell.progress.frame, colors: [UIColor.blue.cgColor, UIColor.red.cgColor], locations: nil) {
            cell.progress.progressImage = gradientImage
            cell.progress.setProgress(1, animated: true)
        }
        
        return cell
    }
    
}
