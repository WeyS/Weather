//
//  NetworkService.swift
//  Weather
//
//  Created by Maxim on 23.05.2022.
//

import Foundation

class NetworkService {
    
    static let shared = NetworkService()
    
    func fetch<T: Decodable>(url: URL ,completion: @escaping (T) -> Void){
        let task = URLSession.shared.dataTask(with: url) { (data, response , error) in
            if let err = error {
                print("Loading data error: \(err.localizedDescription)")
            }else{
                guard let data = data, let decoded = try? JSONDecoder().decode(T.self, from: data) else { return }
                completion(decoded)
            }
        }
        task.resume()
    }
    
}
