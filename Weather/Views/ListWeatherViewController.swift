//
//  ViewController.swift
//  Weather
//
//  Created by Maxim on 21.05.2022.
//

import UIKit
import RxSwift
import RxCocoa

class ListWeatherViewController: UIViewController, Storyboardable {
    
    var coordinator: AppCoordinator?
    private var viewModel = ListWeatherViewModel()
    private var bag = DisposeBag()
    @IBOutlet weak var tableView: UITableView!

    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        viewModel.bind = { [unowned self] data in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.mainData.isEmpty{
            viewModel.getLocationData()
        }
    }
    
    func configureNavigationBar() {
        navigationItem.title = viewModel.title
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        let settingsImage = UIImage(systemName: "ellipsis.circle")
        let tintedImage = settingsImage?.withRenderingMode(.alwaysTemplate)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: tintedImage, style: .plain, target: self, action: #selector(settingsButton))
        navigationItem.rightBarButtonItem?.tintColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true

    }
    
    @objc func settingsButton(){
        tableView.reloadData()
        print(viewModel.mainData)
    }

}

extension ListWeatherViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.mainData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherTableViewCell
        cell.city.text = self.viewModel.mainData[indexPath.row].timezone
        cell.labelTemp.text = "\(String(format: "%.0f" , self.viewModel.mainData[indexPath.row].current?.temp?.rounded() ?? -1))°"
        cell.weatherDescription.text = self.viewModel.mainData[indexPath.row].current?.weather?[0].main?.rawValue
        cell.feelsLike.text = "Feels Like : \(String(format: "%.0f" , self.viewModel.mainData[indexPath.row].current?.feelsLike?.rounded() ?? -1))°"
        
        cell.backGroundImage()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.coordinator?.openDetailView(data: self.viewModel.mainData[indexPath.row])
    }
}

