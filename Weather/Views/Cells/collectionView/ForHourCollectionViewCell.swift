//
//  ForHourCollectionViewCell.swift
//  Weather
//
//  Created by Maxim on 04.06.2022.
//

import UIKit

class ForHourCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var temp: UILabel!
    
    
    
}
