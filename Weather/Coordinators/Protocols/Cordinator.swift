//
//  Cordinator.swift
//  Weather
//
//  Created by Maxim on 21.05.2022.
//

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    
    func start()
}
