//
//  SecondCustomTableViewCell.swift
//  Weather
//
//  Created by Maxim on 07.06.2022.
//

import UIKit

class SecondCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    
}
