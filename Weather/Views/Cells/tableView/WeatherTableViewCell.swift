//
//  WeatherTableViewCell.swift
//  Weather
//
//  Created by Maxim on 22.05.2022.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var view: UIView!
    
    func backGroundImage(){
        if weatherDescription.text == WeatherDescription.clear.rawValue {
            view.backgroundColor = UIColor(patternImage: UIImage(named: "clean")!)
        }else if weatherDescription.text == WeatherDescription.rain.rawValue {
            view.backgroundColor = UIColor(patternImage: UIImage(named: "rain")!)
        }else if weatherDescription.text == WeatherDescription.clouds.rawValue {
            view.backgroundColor = UIColor(patternImage: UIImage(named: "cloudy")!)
        }
    }
    
}
