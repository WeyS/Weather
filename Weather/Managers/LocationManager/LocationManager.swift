//
//  LocationManager.swift
//  Weather
//
//  Created by Maxim on 21.05.2022.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa

class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = LocationManager()
    private var locationManager: CLLocationManager = CLLocationManager()
    var requestLocationAuthorizationCallback: ((CLAuthorizationStatus) -> Void)?
    var status = PublishSubject<CLAuthorizationStatus>()
    var coordinats = PublishSubject<(Double, Double)>()

    public func requestLocationAuthorization() {
        self.locationManager.delegate = self

        self.requestLocationAuthorizationCallback = { status in
            if status == .authorizedWhenInUse {
                self.locationManager.requestAlwaysAuthorization()
            }else if status.rawValue == 0 {
                self.locationManager.requestAlwaysAuthorization()
            }
        }
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    // MARK: - CLLocationManagerDelegate
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.requestLocationAuthorizationCallback?(status)
        self.status.on(.next(status))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.coordinats.on(.next((locValue.latitude, locValue.longitude)))
    }
}
