//
//  AppCoordinator.swift
//  Weather
//
//  Created by Maxim on 21.05.2022.
//

import UIKit

class AppCoordinator: Coordinator {
    var navigationController: UINavigationController

    init (navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ListWeatherViewController.createObject()
        vc.coordinator = self
        
        navigationController.pushViewController(vc, animated: true)
    }
    
    func openDetailView(data: MainWeather) {
        let vc = DetailViewController.createObject()
        vc.coordinator = self
        
        vc.cityData = data
        
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController.present(vc, animated: true, completion: nil)
    }
    
}
